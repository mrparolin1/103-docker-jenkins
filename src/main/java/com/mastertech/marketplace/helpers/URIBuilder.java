package com.mastertech.marketplace.helpers;

import java.net.URI;
import java.net.URISyntaxException;

public class URIBuilder {

	public static URI fromString(String string) {
		try {
			return new URI(string);
		} catch (URISyntaxException e) {
			e.printStackTrace();
			return null;
		}
	}
}
